package lk.dialog.gtpd.training.tdd.controllers;

import lk.dialog.gtpd.training.tdd.operations.SmsSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisitTriggerController {

    private Logger logger = LogManager.getLogger(getClass());

    private SmsSender smsSender = new SmsSender();

    @GetMapping(path = "/visited/{number}")
    public String onCustomerVisit(@PathVariable("number") String number) {
        logger.info("Reqeusted for SMS from {}", number);

        smsSender.sendSms(number);
        return "OK";
    }
}
